DIRS = src tests

all: $(DIRS)
	for dir in $(DIRS) ; do $(MAKE) -C $$dir all ; done

clean: $(DIRS)
	for dir in $(DIRS) ; do $(MAKE) -C $$dir clean ; done

