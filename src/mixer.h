/*
 * File:        mixer.h
 * Created:     2013/12/19
 * Description: api declarations for mixing sound files
 */

#ifndef _MIXER_H_
#define _MIXER_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

    #define MIXER_MAX_SOURCES_PER_MIX 8

    typedef void * MIXER;

    MIXER mixer_new(void);
    bool  mixer_free(MIXER *mixer);
    bool  mixer_add_source(MIXER mixer, const char *file_path, uint32_t signal_weight);
    bool  mixer_clear_sources(MIXER mixer);
    bool  mixer_combine_sources(MIXER mixer, const char *output_path);

#ifdef __cplusplus
}
#endif

#endif // _MIXER_H_

