#include <mixer.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    MIXER    mixer_ctx = NULL;
    uint32_t i         = 0;
    bool     failed    = false;

    mixer_ctx = mixer_new();
    if (!mixer_ctx)
    {
        fprintf(stderr, "failed to create mixer\n");
        failed = true;
        goto CLEANUP;
    }

    for (i = 2; i < argc; i++)
    {
        if (!mixer_add_source(mixer_ctx, argv[i], 1))
        {
            fprintf(stderr, "failed to add source [%s]\n", argv[i]);
            failed = true;
            goto CLEANUP;
        }
    }

    if (!argv[1])
    {
        fprintf(stderr, "invalid output path\n");
        failed = true;
        goto CLEANUP;
    }

    if (!mixer_combine_sources(mixer_ctx, argv[1]))
    {
        fprintf(stderr, "failed to combine sources\n");
        failed = true;
        goto CLEANUP;
    }

CLEANUP:

    mixer_free(&mixer_ctx);

    return failed ? EXIT_FAILURE : EXIT_SUCCESS;
}

