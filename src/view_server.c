/*
 * File:        view_server.c
 * Created:     2013/12/21
 * Description: server for communicating with front-end
 */

#define _GNU_SOURCE

#include <errno.h>
#include <netinet/in.h>
#include <string.h>

#include <glib.h>
#include <glib-unix.h>
#include <libsoup/soup.h>

#define _STRINGIFY(x)       #x
#define STRINGIFY(x)        _STRINGIFY(x)
#define RESP_BUF_LEN        8192
#define DICT_BUF_LEN        512
#define COOKIE_BUF_LEN      128
#define FILE_PATH_BUF_LEN   512
#define MAX_LINE_LEN        256
#define FORMAT_BUF_LEN      128
#define BACK_TRACE_BUF_SIZE 128

static const gchar  *prg_name       = NULL;
static pid_t         pid            = 0;
static int           opt_port       = -1;
static GOptionEntry  main_entries[] = {
  {"port", '\0', 0, G_OPTION_ARG_INT, &opt_port, "Listening port", "[0]"},
  {NULL}};

typedef struct view_server_ctx
{
  SoupServer *server;
  guint       sig_term_id;
  guint       sig_int_id;

} view_server_ctx;

static void handle_view_test(SoupServer *server, SoupMessage *msg,
    const char *path, GHashTable *query, SoupClientContext *client,
    gpointer user_data);
static void handle_view_posts(SoupServer *server, SoupMessage *msg,
    const gchar *path, GHashTable *query, SoupClientContext *client,
    gpointer user_data);
static void on_crash(int sig);
static gboolean on_signal(gpointer user_data);

static void
handle_view_test(SoupServer        *server,
                 SoupMessage       *msg,
                 const gchar       *path,
                 GHashTable        *query,
                 SoupClientContext *client,
                 gpointer           user_data)
{
  view_server_ctx *ctx                    = (view_server_ctx *)user_data;
  gchar            response[RESP_BUF_LEN] = {0};
  gint             n                      = 0;
  gboolean         failed                 = FALSE;
  SoupStatus       status                 = SOUP_STATUS_NONE;

  g_return_if_fail(NULL != msg);
  g_return_if_fail(NULL != msg->request_headers);
  g_return_if_fail(NULL != ctx);

  n = g_snprintf(response, sizeof(response),
          "<html><head><title>View Test</title></head><body>It Works!!</body></html>");

  soup_message_headers_replace(msg->response_headers,
      "Content-Type", "text/plain");
  soup_message_set_response(msg, "text/html", SOUP_MEMORY_COPY,
      response, strnlen(response, sizeof(response)));
  status = SOUP_STATUS_OK;
  soup_message_set_status(msg, status);
}

static void
handle_view_posts(SoupServer        *server,
                  SoupMessage       *msg,
                  const gchar       *path,
                  GHashTable        *query,
                  SoupClientContext *client,
                  gpointer           user_data)
{
  view_server_ctx *ctx                    = (view_server_ctx *)user_data;
  gchar            response[RESP_BUF_LEN] = {0};
  gint             n                      = 0;
  gboolean         failed                 = FALSE;
  SoupStatus       status                 = SOUP_STATUS_NONE;

  g_return_if_fail(NULL != msg);
  g_return_if_fail(NULL != msg->request_headers);
  g_return_if_fail(NULL != ctx);

  n = g_snprintf(response, sizeof(response),
          "<html><head><title>Posts</title></head><body>Under Construction</body></html>");
  g_return_if_fail(0 < n && sizeof(response) > n);

  soup_message_headers_replace(msg->response_headers,
      "Content-Type", "text/plain");
  soup_message_set_response(msg, "text/html", SOUP_MEMORY_COPY,
      response, strnlen(response, sizeof(response)));
  status = SOUP_STATUS_OK;
  soup_message_set_status(msg, status);
}

static void on_crash(int sig)
{
  void    *buffer[BACK_TRACE_BUF_SIZE] = {0};
  size_t   len                         = 0;
  size_t   nptrs                       = 0;
  char   **strings                     = NULL;
  size_t   i                           = 0;

  g_debug("signal %d (%s)", sig, sys_siglist[sig]);

  len   = sizeof(buffer) / sizeof(void *);
  nptrs = backtrace(buffer, len);

  g_warning("backtrace() returned %d addresses %s", nptrs,
      (nptrs == len ? "(may have been truncated)" : ""));

  strings = backtrace_symbols(buffer, nptrs);
  if (strings == NULL)
  {
    g_warning("backtrace_symbols failed");
    goto TERMINATE;
  }

  for (i = 0; i < nptrs; i++)
    g_warning("%s", strings[i]);

  free(strings);

  signal(sig, SIG_DFL);
  kill(getpid(), sig);

 TERMINATE:

  exit(EXIT_SUCCESS);
}

static gboolean on_signal(gpointer user_data)
{
  view_server_ctx *ctx = (view_server_ctx *)user_data;

  g_return_val_if_fail(NULL != ctx, G_SOURCE_REMOVE);
  g_return_val_if_fail(NULL != ctx->server, G_SOURCE_REMOVE);

  g_debug("\rCaught signal, stopping View Server...");

  soup_server_quit(ctx->server);

  return G_SOURCE_REMOVE;
}

int main(int argc, char *argv[])
{
  view_server_ctx     ctx          = {};
  GOptionContext     *opt_ctx      = NULL;
  struct sockaddr_in  lo_addr      = {};
  SoupAddress        *soup_lo_addr = NULL;
  sighandler_t        sig_handler  = NULL;
  int                 ret          = 0;
  gboolean            failed       = FALSE;
  GError             *error        = NULL;

  opt_ctx = g_option_context_new("View Server");
  g_option_context_add_main_entries(opt_ctx , main_entries, NULL);

  if (FALSE == g_option_context_parse(opt_ctx , &argc, &argv, &error))
  {
    g_warning("failed to parse options, error: [%s]\n", error->message);
    failed = TRUE;
    goto CLEANUP;
  }

  if (0 >= opt_port || 65535 < opt_port)
  {
    g_warning("invalid port range: [%d]", opt_port);
    failed = TRUE;
    goto CLEANUP;
  }

  prg_name = g_get_prgname();
  pid      = getpid();

  sig_handler = signal(SIGSEGV, on_crash);
  g_assert(SIG_ERR != sig_handler);
  sig_handler = signal(SIGBUS, on_crash);
  g_assert(SIG_ERR != sig_handler);

  ctx.sig_term_id = g_unix_signal_add(SIGTERM, on_signal, &ctx);
  g_assert(0 < ctx.sig_term_id);
  ctx.sig_int_id = g_unix_signal_add(SIGINT, on_signal, &ctx);
  g_assert(0 < ctx.sig_int_id);

  lo_addr.sin_family      = AF_INET;
  lo_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
  lo_addr.sin_port        = htons((short)opt_port);
  soup_lo_addr            = soup_address_new_from_sockaddr(&lo_addr, sizeof(lo_addr));
  if (!soup_lo_addr)
  {
    g_warning("failed to create loopback SoupAddress");
    failed = TRUE;
    goto CLEANUP;
  }

  ctx.server = soup_server_new(SOUP_SERVER_INTERFACE, soup_lo_addr,
          SOUP_SERVER_PORT, opt_port, NULL);
  g_assert(NULL != ctx.server);

  soup_server_add_handler(ctx.server, "/view/test",
      handle_view_test, &ctx, NULL);
  soup_server_add_handler(ctx.server, "/view/posts",
      handle_view_posts, &ctx, NULL);

  g_message("Starting View Server, listening on port [%d]", opt_port);

  soup_server_run(ctx.server);

 CLEANUP:

  g_message("Destroying View Server...");

  g_clear_error(&error);

  if (ctx.sig_term_id)
    g_source_remove(ctx.sig_term_id);
  if (ctx.sig_int_id)
    g_source_remove(ctx.sig_int_id);

  g_clear_pointer(&soup_lo_addr, g_object_unref);
  g_clear_pointer(&ctx.server, soup_server_disconnect);
  g_clear_pointer(&opt_ctx, g_option_context_free);

  return (failed ? EXIT_FAILURE : EXIT_SUCCESS);
}

