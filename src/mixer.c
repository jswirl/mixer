/*
 * File:        mixer.c
 * Created:     2013/12/19
 * Description: api implementation for mixing sound files
 */

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <sndfile.h>

#include <mixer.h>
#include <common.h>
#include <macros.h>

#define MAX_FRAMES_PER_READ 2048

typedef struct source_t
{
    SF_INFO   info;
    SNDFILE  *file;
    char      file_path[MAX_FILE_PATH_LEN + 1];
    uint32_t  signal_weight;

} source_t;

typedef struct mixer_t
{
    source_t output;
    source_t sources[MIXER_MAX_SOURCES_PER_MIX];
    uint32_t source_count;
    uint32_t output_frames;
    int32_t  mixer_error;

} mixer_t;

static bool mixer_open_sources(MIXER mixer_ctx);
static bool mixer_open_output(MIXER mixer_ctx, const char *output_path);
static bool mixer_parse_source_infos(MIXER mixer_ctx, SF_INFO *out_info);

MIXER mixer_new(void)
{
    mixer_t *mixer = (mixer_t *)(calloc(1, sizeof(mixer_t)));
    return (MIXER)(mixer);
}

bool mixer_free(MIXER *mixer_ctx)
{
    mixer_t *mixer  = NULL;
    bool     failed = false;

    RET_VAL_IF_FAIL(mixer_ctx, false);
    RET_VAL_IF_FAIL(*mixer_ctx, false);

    mixer = (mixer_t *)(*mixer_ctx);

    mixer_clear_sources(mixer);
    if ((mixer->output).file)
    {
        if (sf_close((mixer->output).file) != 0)
            failed = true;

        (mixer->output).file = NULL;
    }

    FREE_AND_NULLIFY(*mixer_ctx);

    return !failed;
}

bool mixer_add_source(MIXER mixer_ctx, const char *file_path, uint32_t signal_weight)
{
    mixer_t  *mixer  = (mixer_t *)(mixer_ctx);
    source_t *source = NULL;

    RET_VAL_IF_FAIL(mixer, false);
    RET_VAL_IF_FAIL(file_path, false);
    RET_VAL_IF_FAIL(signal_weight > 0, false);
    RET_VAL_IF_FAIL(mixer->source_count < MIXER_MAX_SOURCES_PER_MIX, false);

    source = &((mixer->sources)[mixer->source_count]);
    NULL_TERM_STRNCPY(source->file_path, file_path, sizeof(source->file_path));
    source->signal_weight = signal_weight;
    ++(mixer->source_count);

    return true;
}

bool mixer_clear_sources(MIXER mixer_ctx)
{
    mixer_t  *mixer   = (mixer_t *)(mixer_ctx);
    source_t *source  = NULL;
    uint32_t  src_idx = 0;
    int       ret     = 0;
    bool      failed  = false;

    RET_VAL_IF_FAIL(mixer, false);

    for (src_idx = 0; src_idx < MIXER_MAX_SOURCES_PER_MIX; src_idx++)
    {
        source = &((mixer->sources)[src_idx]);

        if (!(source->file))
            continue;

        ret = sf_close(source->file);
        if (ret != 0)
        {
            failed = true;
            continue;
        }

        memset(source, 0, sizeof(source_t));
    }

    mixer->source_count = 0;

    return !failed;
}

/* currently only PCM 16 bits per sample is supported */
bool mixer_combine_sources(MIXER mixer_ctx, const char *output_path)
{
    mixer_t    *mixer      = (mixer_t *)(mixer_ctx);
    source_t   *source     = NULL;
    int16_t    *frame_buf  = NULL;
    int32_t    *sums       = NULL;
    uint32_t    weight_sum = 0;
    uint32_t    frame_idx  = 0;
    uint32_t    src_idx    = 0;
    uint32_t    buf_idx    = 0;
    sf_count_t  out_cnt    = 0;
    sf_count_t  n          = 0;
    bool        failed     = false;

    RET_VAL_IF_FAIL(mixer, false);
    RET_VAL_IF_FAIL(output_path, false);

    if (!mixer_open_sources(mixer))
    {
        failed = true;
        goto CLEANUP;
    }

    NULL_TERM_STRNCPY((mixer->output).file_path, output_path, sizeof((mixer->output).file_path));

    if (!mixer_open_output(mixer, output_path))
    {
        failed = true;
        goto CLEANUP;
    }

    if ((mixer->output).info.format != (SF_FORMAT_WAV | SF_FORMAT_PCM_16))
    {
        failed = true;
        goto CLEANUP;
    }

    frame_buf = (int16_t *)(calloc((mixer->output).info.channels * MAX_FRAMES_PER_READ, sizeof(int16_t)));
    if (!frame_buf)
    {
        failed = true;
        goto CLEANUP;
    }

    sums = (int32_t *)(calloc((mixer->output).info.channels * MAX_FRAMES_PER_READ, sizeof(int32_t)));
    if (!sums)
    {
        failed = true;
        goto CLEANUP;
    }

    for (src_idx = 0; src_idx < mixer->source_count; src_idx++)
    {
        source = &((mixer->sources)[src_idx]);
        weight_sum += source->signal_weight;
    }

    for (frame_idx = 0; frame_idx < mixer->output_frames; frame_idx += out_cnt)
    {
        memset(frame_buf, 0, sizeof(int16_t) * (mixer->output).info.channels * MAX_FRAMES_PER_READ);
        memset(sums, 0, sizeof(int32_t) * (mixer->output).info.channels * MAX_FRAMES_PER_READ);

        for (src_idx = 0, out_cnt = 0; src_idx < mixer->source_count; src_idx++)
        {
            source  = &((mixer->sources)[src_idx]);
            n       = sf_readf_short(source->file, frame_buf, MAX_FRAMES_PER_READ);
            out_cnt = n > out_cnt ? n : out_cnt;

            if (n == 0)
                continue;

            if (n < 0)
            {
                failed = true;
                goto CLEANUP;
            }

            for (buf_idx = 0; buf_idx < n * (mixer->output).info.channels; buf_idx++)
                sums[buf_idx] += source->signal_weight * frame_buf[buf_idx];
        }

        for (buf_idx = 0; buf_idx < out_cnt * (mixer->output).info.channels; buf_idx++)
            frame_buf[buf_idx] = (int16_t)(sums[buf_idx] / weight_sum);

        n = sf_writef_short((mixer->output).file, frame_buf, out_cnt);
        if (n != out_cnt)
        {
            failed = true;
            goto CLEANUP;
        }
    }

CLEANUP:

    FREE_AND_NULLIFY(frame_buf);
    FREE_AND_NULLIFY(sums);
    if ((mixer->output).file && sf_close((mixer->output).file) != 0)
        failed = true;
    (mixer->output).file = NULL;

    return !failed;
}

static bool mixer_open_sources(MIXER mixer_ctx)
{
    mixer_t  *mixer   = (mixer_t *)(mixer_ctx);
    source_t *source  = NULL;
    uint32_t  src_idx = 0;
    bool      failed  = false;

    RET_VAL_IF_FAIL(mixer, false);

    for (src_idx = 0; src_idx < mixer->source_count; src_idx++)
    {
        source       = &((mixer->sources)[src_idx]);
        source->file = sf_open(source->file_path, SFM_READ, &(source->info));
        if (!(source->file))
        {
            failed = true;
            break;
        }
    }

    if (failed)
        mixer_clear_sources(mixer_ctx);

    return !failed;
}

static bool mixer_open_output(MIXER mixer_ctx, const char *output_path)
{
    mixer_t *mixer    = (mixer_t *)(mixer_ctx);
    SF_INFO *out_info = NULL;

    RET_VAL_IF_FAIL(mixer, false);
    RET_VAL_IF_FAIL(output_path, false);

    out_info = &((mixer->output).info);

    if (!mixer_parse_source_infos(mixer, out_info))
        return false;

    (mixer->output).file = sf_open((mixer->output).file_path, SFM_RDWR, out_info);
    if (!((mixer->output).file))
        return false;

    return true;
}

static bool mixer_parse_source_infos(MIXER mixer_ctx, SF_INFO *out_info)
{
    mixer_t  *mixer    = (mixer_t *)(mixer_ctx);
    source_t *source   = NULL;
    SF_INFO  *src_info = NULL;
    uint32_t  src_idx  = 0;

    RET_VAL_IF_FAIL(mixer, false);
    RET_VAL_IF_FAIL(out_info, false);

    source   = &((mixer->sources)[0]);
    src_info = &(source->info);
    memcpy(out_info, src_info, sizeof(SF_INFO));

    for (src_idx = 1; src_idx < mixer->source_count; src_idx++)
    {
        source   = &((mixer->sources)[src_idx]);
        src_info = &(source->info);

        if (src_info->frames > mixer->output_frames)
            mixer->output_frames = src_info->frames;

        if (src_info->samplerate != out_info->samplerate ||
            src_info->channels   != out_info->channels   ||
            src_info->format     != out_info->format     ||
            src_info->sections   != out_info->sections   ||
            src_info->seekable   != out_info->seekable)
            return false;
    }

    return true;
}

