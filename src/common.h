/*
 * File:        common.h
 * Created:     2013/12/19
 * Description: common value definitions
 */

#ifndef _COMMON_H_
#define _COMMON_H_

#define MAX_FILE_PATH_LEN 256

#endif // _COMMON_H_
