/*
 * File:        macros.h
 * Created:     2013/12/19
 * Description: common macros
 */

#ifndef _MACROS_H_
#define _MACROS_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <errno.h>
#include <stdarg.h>

#ifdef __cplusplus
extern "C"
{
#endif

    #define __gnuc_va_list va_list 
    #define unlikely(x) __builtin_expect(!!(x), 0)
    #define STRINGIFY(x) _STRINGIFY(x)
    #define _STRINGIFY(x) #x
    #define FREE_AND_NULLIFY(ptr) {if (ptr) {free(ptr); ptr = NULL;}}
    #define FCLOSE_AND_NULLIFY(ptr) {if (ptr) {fclose(ptr); ptr = NULL;}}
    #define SET_VAR_JMP_LBL(ret, err, lbl) {ret = err; goto lbl;}
    #define NULL_TERM_STRNCPY(buf, src, buf_len) {strncpy(buf, src, buf_len); buf[buf_len - 1] = '\0';}
    #define RETURN_IF_FAIL(x) {if (unlikely(!(x))) {fprintf(stderr, "!(%s) @ line %d of %s()\n", #x, __LINE__, __func__); return;}}
    #define RET_VAL_IF_FAIL(x, ret) {if (unlikely(!(x))) {fprintf(stderr, "!(%s) @ line %d of %s()\n", #x, __LINE__, __func__); return ret;}}
    #define RET_SET_VAR_IF_FAIL(x, var, val) {if (unlikely(!(x))) {fprintf(stderr, "!(%s) @ line %d of %s()\n", #x, __LINE__, __func__); var = val; return;}}
    #define JMP_LBL_IF_FAIL(x, lbl) {if (unlikely(!(x))) {fprintf(stderr, "!(%s) @ line %d of %s()\n", #x, __LINE__, __func__); goto lbl;}}
    #define SET_VAR_JMP_LBL_IF_FAIL(x, var, val, lbl) {if (unlikely(!(x))) {fprintf(stderr, "!(%s) @ line %d of %s()\n", #x, __LINE__, __func__); SET_VAR_JMP_LBL(var, val, lbl);}}

#ifdef __cplusplus
}
#endif

#endif // _MACROS_H_

